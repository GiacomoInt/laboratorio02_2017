package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private static final int MAX_ROLLS = 20;
	private int[] rolls = new int[22];
	private int currentRoll = 0;
	@Override
	public void roll(int pins) {
		rolls[currentRoll++] = pins;

	}

	@Override
	public int score() {
		int score = 0;
		for(int currentRoll = 0; currentRoll < MAX_ROLLS; currentRoll++){
			if ( isSpare(currentRoll))
				score += rolls[currentRoll+2];
			if (isStrike(currentRoll)){
				score += rolls[currentRoll+1]+rolls[currentRoll+2];
			}	
			score += rolls[currentRoll];
		}

		score+= rolls[21];
		return score;
	}

	private boolean isSpare(int currentRoll) {
		if(currentRoll%2 == 0 && rolls[currentRoll+1] != 0)
			return currentRoll < MAX_ROLLS -1 && rolls[currentRoll] + rolls[currentRoll+1] == 10;
		else 
			return false;
			
	}
	private boolean isStrike(int currentRoll) {
		if(currentRoll%2 == 0 ){
			return rolls[currentRoll] == 10;
		}else 
			return false;
			
	}

}
