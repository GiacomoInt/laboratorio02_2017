package it.unimi.di.sweng.lab02;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class BowlingTest {

	private static final int MAX_ROLLS = 20;

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private Bowling game;

	@Before
	public void setUp(){
		game = new BowlingGame();
	}
	
	@Test
	public void gutterGame() {
		int j=20;
		int pins =0;
		rollMany(20,0);
		assertThat(game.score()).isEqualTo(0);
	}

	private void rollMany(int times, int pins) {
		for(int i=0; i<times; i++)
			game.roll(pins);
	}
	
	@Test
	public void allOnesGame() {
		rollMany(MAX_ROLLS,1);
		assertThat(game.score()).isEqualTo(20);
	}
	
	@Test
	public void oneSpareGame() {
		game.roll(5);
		game.roll(5);
		game.roll(3);
		rollMany(MAX_ROLLS-3,0);
		assertThat(game.score()).isEqualTo(16);
	}
	
	@Test
	public void notSpareGame() {
		game.roll(3);
		game.roll(5);
		game.roll(5);
		game.roll(3);
		rollMany(MAX_ROLLS-4,0);
		assertThat(game.score()).isEqualTo(16);
	}
	
	@Test
	public void oneStrikeGame() {
		game.roll(10);
		game.roll(3);
		game.roll(4);
		rollMany(MAX_ROLLS-3,0);
		assertThat(game.score()).isEqualTo(24);
	}
	
	@Test
	public void notStrikeGame() {
		game.roll(0);
		game.roll(10);
		game.roll(3);
		rollMany(MAX_ROLLS-3,0);
		assertThat(game.score()).isEqualTo(16);
		
	}
	
	@Test
	public void lastFrameStrikeGame() {
		rollMany(MAX_ROLLS-1,0);
		game.roll(10);
		game.roll(3);
		game.roll(2);
		assertThat(game.score()).isEqualTo(15);
	}
	
	@Test
	public void perfectGame() { 
		rollMany(MAX_ROLLS,10);
		assertThat(game.score()).isEqualTo(300);
	}
	
/**/
	
}
